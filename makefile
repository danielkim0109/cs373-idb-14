# get git config
config:
	git config -l

# get git log

cs373-idb-14.log.txt:
	git log > cs373-idb-14.log.txt

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the repo
pull:
	@echo
	git pull
	git status
