Group Members (EID, GitLab ID):

- Hussain Murtaza / hm25354 / @hmurtaza5191
- Phuc Q Dang / pqd76 / @peaetchyousee
- Kaushik Jampala / kj23624 / @kjampala
- Andrew Jaso / avj349 / @AndrewJaso
- Daniel Kim / ik3743 / @danielkim0109

Git SHA:  a7abdd20da09f0f88e35cc4860d8c2cf405a3955
Project Leader Phase 1: Hussain
Project Leader Phase 2: Phuc
Project Leader Phase 3: Kaushik
Project Leader Phase 4: Andrew


Project Leader Responsibility: 
Check up on all aspects of the project, plan team meetings, communicate with the graders if there are any questions.

GitLab Pipelines: https://gitlab.com/danielkim0109/cs373-idb-14/-/pipelines

Website: https://www.symptomsolver.me

Estimated completion time for each member:

- Hussain - 10 hours
- Phuc - 10 hours
- Kaushik - 10 hours
- Andrew - 10 hours
- Daniel - 10 hours

Actual completion time for each member:

- Hussain - 13 hours
- Phuc - 12 hours
- Kaushik - 12 hours
- Andrew - 15 hours
- Daniel - 14 hours

Comments:
Citation: GeoJobs - https://gitlab.com/sarthaksirotiya/cs373-idb


Phase 2:

Git SHA: fc66a7811ee37fefdccdf8916a9efe4919e7caae

Estimated completion time for each member:

- Hussain - 16 hours
- Phuc - 18 hours
- Kaushik - 14 hours
- Andrew - 15 hours
- Daniel - 17 hours

Actual completion time for each member:

- Hussain - 18 hours
- Phuc - 20 hours
- Kaushik - 17 hours
- Andrew - 18 hours
- Daniel - 20 hours

Comments:
Used code:

https://gitlab.com/mihikabirmiwal/cs373-idb/-/blob/develop/front-end/guitests.py
https://gitlab.com/mihikabirmiwal/cs373-idb/-/blob/develop/front-end/gui_tests/navbarTests.py
https://gitlab.com/jaegarciah/cs373-idb-19/-/blob/main/front-end/flow-tests/seleniumTests.py

Phase 3:

Git SHA: 9c7179a6686c77bdf31eda90c72827fd15d310cb

GitLab Pipelines: https://gitlab.com/danielkim0109/cs373-idb-14/-/pipelines

Estimated completion time for each member:

- Hussain - 18 hours
- Phuc - 19 hours
- Kaushik - 17 hours
- Andrew - 18 hours
- Daniel - 20 hours

Actual completion time for each member:

- Hussain - 19 hours
- Phuc - 20 hours
- Kaushik - 17 hours
- Andrew - 18 hours
- Daniel - 21 hours

Phase 4:

Git SHA: d87ca27e5d539612fc1c2136338ee1c84941991f

GitLab Pipelines: https://gitlab.com/danielkim0109/cs373-idb-14/-/pipelines

Estimated completion time for each member:

- Hussain - 5 hours
- Phuc - 4 hours
- Kaushik - 5 hours
- Andrew - 4 hours
- Daniel - 5 hours

Actual completion time for each member:

- Hussain - 5 hours
- Phuc - 5 hours
- Kaushik - 4 hours
- Andrew - 4 hours
- Daniel - 5 hours

Comments:
Citation: GeoJobs - https://gitlab.com/sarthaksirotiya/cs373-idb
