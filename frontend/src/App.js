import React from 'react';
import './App.css';
import Router from './Routing.js';
import SiteNavbar from './components/SiteNavbar.js';



const App = () => {
  return (
    <div style={{backgroundColor: 'lightsteelblue', width: '100vw', height: '100%'}}>
      <SiteNavbar />
      <Router />
    </div>
  )
}
export default App;
