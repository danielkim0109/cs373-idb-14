import os
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service
import sys

URL = 'https://www.symptomsolver.me/'
PATH = 'gui_tests/chromedriver'

class NavbarTests(unittest.TestCase):

    # Get drivers and run website before all tests
    @classmethod
    def setUpClass(self):
        options = webdriver.ChromeOptions()
        options.add_experimental_option('excludeSwitches', ['enable-logging'])
        options.add_argument("--headless")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--window-size=1440, 900")
        chrome_prefs = {}
        options.experimental_options["prefs"] = chrome_prefs
        self.driver = webdriver.Chrome(options = options, service = Service(ChromeDriverManager().install()))
        self.driver.get(URL)


    # Close browser and quit after all tests
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

    def testAbout(self):
        element = self.driver.find_elements(By.LINK_TEXT, "About")[0]
        newURL = element.get_attribute("href")
        print(newURL)
        self.assertEqual(newURL, URL + "about")

    def testArtists(self):
        element = self.driver.find_elements(By.LINK_TEXT, "Symptoms")[0]
        newURL = element.get_attribute("href")
        self.assertEqual(newURL, URL + "symptoms")

    def testNavMedication(self):
        element = self.driver.find_elements(By.LINK_TEXT, "Medications")[0]
        newURL = element.get_attribute("href")
        self.assertEqual(newURL, URL + "medications")

    def testNavLocation(self):
        element = self.driver.find_elements(By.LINK_TEXT, "Locations")[0]
        newURL = element.get_attribute("href")
        self.assertEqual(newURL, URL + "locations")


if __name__ == "__main__":
    PATH = sys.argv[1]
    unittest.main(argv=['first-arg-is-ignored'])


# Code from https://gitlab.com/mihikabirmiwal/cs373-idb/-/blob/develop/front-end/gui_tests/navbarTests.py
# & https://gitlab.com/jaegarciah/cs373-idb-19/-/blob/main/front-end/flow-tests/seleniumTests.py

