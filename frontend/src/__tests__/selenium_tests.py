import os
from sys import platform

if __name__ == '__main__':
    if platform == "win32":
        PATH = "/gui_tests/chromedriver.exe"
    elif platform == "linux":
        PATH = "/gui_tests/chromedriver_linux"
    elif platform == "darwin":
        PATH = "/gui_tests/chromedriver"
    else:
        print("platform", platform)
        print("Unsupported OS")
        exit(-1)

    os.system("echo Running Navbar Tests...")
    os.system("python3 gui_tests/navbar_tests.py " + PATH)


# Code from https://gitlab.com/mihikabirmiwal/cs373-idb/-/blob/develop/front-end/guitests.py