import React, {useEffect, useState} from 'react';

import '../styles/Views.css'

import DeveloperCard from '../components/DeveloperCard';
import ToolsCard from '../components/ToolsCard';

import { groupInfo } from '../components/GroupInfo';
import { tools, apis} from '../components/Tools';

import Container from 'react-bootstrap/esm/Container';
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Stack from "react-bootstrap/Stack";




const getGitLabInfo = async () => {
  let numCommits = 0, numIssues = 0, numUnitTests = 0;

  groupInfo.forEach((member)=>
  {
    member.commits = 0;
    member.issues = 0;  
    numUnitTests += member.unitTests;
  });

  await fetch('https://gitlab.com/api/v4/projects/43386619/repository/contributors?private_token=glpat-g2fSFuVq4ZedWTxqkQ1u')
    .then(response => response.json())
    .then(data => data.forEach((element) => {
      const {name, email, commits} = element;
      groupInfo.forEach((member) => {
        if (member.name === name || member.email === email)
        {
          member.commits = commits
        }
        
      }
      );
      numCommits += commits;
    }));
  
  await fetch('https://gitlab.com/api/v4/projects/43386619/issues?private_token=glpat-g2fSFuVq4ZedWTxqkQ1u')
    .then(response => response.json())
    .then(data => data.forEach((element) => {
      const { author } = element;
      console.log(author.name);
      groupInfo.forEach((member) => {
        if (member.name === author.name || member.username === author.username)
        {
          console.log(member.name)
          member.issues += 1
        }
        
      });
      numIssues += 1
    }))
  
  return {
    groupInfo: groupInfo,
    numCommits: numCommits,
    numIssues: numIssues,
    numUnitTests: numUnitTests
  };


};


const About = () => {
  const [groupList, setGroupList] = useState([]);
  const [numCommits, setNumCommits] = useState(0);
  const [numIssues, setNumIssues] = useState(0);
  const [numUnitTests, setNumUnitTests] = useState(0);
  useEffect(() => {
    const fetchData = async () => {
      if (groupList === undefined || groupList.length === 0) {
        const gitlabInfo = await getGitLabInfo();
        setGroupList(gitlabInfo.groupInfo);
        setNumCommits(gitlabInfo.numCommits);
        setNumIssues(gitlabInfo.numIssues);
        setNumUnitTests(gitlabInfo.numUnitTests);
      }
    };
    fetchData();
  }, [groupList]);

  
  
  
  return (
    <Stack className='background'>
        <Container>
            <h1 className='about-title'>About Symptom Solver</h1>
            <p className='descr'>Symptom Solver highlights illnesses, 
                symptoms, and healthcare locations.<br/><br/>
                This site is intended for users who would like to receive information 
                about these particular diseases, and 
                also possibly find treatment. Users can also diagnose themselves
                using symptoms they may be seeing and take action
                accordingly. The site also contains a list of healthcare locations 
                where users may want to go to receive treatment.</p>    
        </Container>
        <Container>
          <Row> {groupList.map((member) => {
            return (
            <Col className="d-flex align-self-stretch">
              <DeveloperCard memberInfo = {member} />
            </Col>)
          })}
          </Row>  
        </Container>
        <Container className='gitlab'>
          <Row>
            <h2>Total Commits: {numCommits}</h2>
            <h2>Total Issues: {numIssues}</h2>
            <h2>Total Unit Tests: {numUnitTests}</h2>
            <h2><a href='https://documenter.getpostman.com/view/25877762/2s93CHtEza'>API Documentation</a></h2>
          </Row>
        </Container>
        <Container className="p-4">
          <Row>
            <h2>Tools Used</h2>
          </Row>
          <Row xs={4} className="g-4 justify-content-center p-4"> {tools.map((tool) => {
            return (
            <Col>
              <ToolsCard toolInfo = {tool} />
            </Col>)
          })}
          </Row>  
        </Container>
        <Container>
          <Row>
            <h2>APIs Used</h2>
          </Row>
          <Row> {apis.map((api) => {
            return (
            <Col>
              <ToolsCard toolInfo = {api} />
            </Col>)
          })}
          </Row>  
        </Container>
    </Stack>
  )
}
export default About;
