import React from 'react';

import Stack from "react-bootstrap/Stack";
import Container from 'react-bootstrap/esm/Container';
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";



import '../styles/Views.css'
import ModelsCard from '../components/ModelsCard';

import { models } from '../components/ModelInfo';



const Home = () => {
    return (
        
        <Stack>
            <Container className='background-img'>
                <h1 className='home-title'>Symptom Solver</h1>
            </Container>
        
            <Container>
                <Row> {models.map((model) => {
                    return (
                    <Col >
                    <ModelsCard modelInfo = {model} />
                    </Col>)
                })}
                </Row>  
            </Container>
        </Stack>
    )
}

export default Home;