import React from 'react';
import Stack from 'react-bootstrap/Stack';
import LocationVisual from '../components/LocationVisual';


import '../styles/Views.css'
import MedicationsVisual from '../components/MedicationsVisual';
import CityVisual from '../components/CityVisual';
import RecipesVisual from '../components/RecipesVisual';
import RestaurantVisual from '../components/RestaurantsVisual';
import SymptomVisual from '../components/SymptomVisual';



const Visualizations = () => {

    return (
        <Stack>
            <h1 style={{ textAlign: "center" }}>Visualizations</h1>
            <LocationVisual/>
            <MedicationsVisual/>
            <SymptomVisual/>
            <h1 style={{ textAlign: "center" }}>Provider Visualizations</h1>
            <CityVisual/>
            <RecipesVisual/>
            <RestaurantVisual/>
        </Stack>
    );
}

export default Visualizations