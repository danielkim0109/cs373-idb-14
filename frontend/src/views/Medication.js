import React, { useState, useEffect, useRef } from "react";

import Container from 'react-bootstrap/esm/Container';
import {Row, Col, Form, FloatingLabel, InputGroup} from 'react-bootstrap'
import Pagination from "react-bootstrap/Pagination";
import Button from "react-bootstrap/Button";
import MedicationCard from '../components/MedicationCard';
import Filters from '../components/Filters';

import axios from 'axios';

const client = axios.create({
    baseURL: "https://api.symptomsolver.me",
});

let queryRE = null;

const Medication = () => {
    const [medicines, setMedicines] = useState([]);
    
    const [loaded, setLoaded] = useState(false);
    const [activePage, setActivePage] = useState(1);
    const [data_length, setdata_length] = useState(122);

    const [sort, setSort] = useState("sort")
    const [manufacturer, setManufacturer] = useState("manufacturer")
    const [route, setRoute] = useState("route")
    const [form, setForm] = useState("form")

    const searchQuery = useRef("");

    function handleClick(page_num) {
        setActivePage(page_num);
        setLoaded(false);
    }

    const handleSortFilter = (value) => {
        setSort(value.toLowerCase());
        setLoaded(false);
    };

    const handleManufacturerFilter = (value) => {
        setManufacturer(value.toLowerCase());
        setLoaded(false);
    };
    
    const handleRouteFilter = (value) => {
        setRoute(value.toLowerCase());
        setLoaded(false);
    };
    
    const handleFormFilter = (value) => {
        setForm(value.toLowerCase());
        setLoaded(false);
    };

    function handleReset () {
        setSort("sort");
        setManufacturer("manufacturer");
        setRoute("route");
        setForm("form");
        setLoaded(false);

    }
    const layout = [
        {
            label: "Sort",
            default: "none",
            options: ["ascending", "descending"],
            onclick: handleSortFilter
        },
        {
            label: "Major Manufacturers",
            default: "none",
            options: ["ALL BETTER CBD", "WALMART", "AiPing", "Teva Parenteral", "Advanced Rx", "Exeltis",
                    "International Medication Systems", "Sun Pharmaceutical"],
            onclick: handleManufacturerFilter
        },
        {
            label: "Route",
            default: "none",
            options: ["Oral", "Topical", "Respiratory", "Intravenous", "Intrathecal"],
            onclick: handleRouteFilter
        },
        {
            label: "Form",
            default: "none",
            options: ["Tablet", "Gel", "Injection",  "Gas", "Capsule", "Stick", "Powder", "Jelly", "Solution", "Aerosol", "Cream"],
            onclick: handleFormFilter
        }
    ]
    useEffect(()=>{
        const fetchMedicines = async () => {
            if (!loaded) {
                let query=`treatments?page=${activePage}`
                if (searchQuery.current.value != "") {
                    query = `/treatments?search=${searchQuery.current.value}`;
                    queryRE = new RegExp(`(?:${searchQuery.current.value.replaceAll(" ", "|")})`,
                    "i"
                  );
                }
                if (sort !== "sort") {
                    query += `&sort=${sort}`;
                }
                if (manufacturer !== "manufacturer") {
                    query += `&manufacturer=${manufacturer}`;
                }
                if (route !== "route") {
                    query += `&route=${route}`;
                }
                if (form !== "form") {
                    query += `&form=${form}`;
                }
                console.log(query)
                await client
                  .get(query)
                  .then((response) => {
                    let numPages = response.data.length / 12;
                    let page_start = response.data.length / numPages * (activePage - 1);
                    let page_end = response.data.length / numPages * activePage;
                    console.log(response.data.length)
                    setMedicines(response.data.slice(page_start, page_end));
                    setdata_length(response.data.length);
                  })
                  .catch((err) => console.log(err));
                setLoaded(true);
              }
        };
        fetchMedicines();
    }, [medicines, loaded])

    let numPages = data_length / 12;
    let items = [];
    for (let page_num = 1; page_num <= numPages + .99; page_num++) {
        items.push(
            <Pagination.Item
            key={page_num}
            onClick={() => handleClick(page_num)}
            active={page_num === activePage}
            >
            {page_num}
            </Pagination.Item>
        );
    }
    

    return (
        <div>
            <Container>
            <Container className="my-5">
                <InputGroup className="mb-3">
                    <FloatingLabel
                        controlId="searchInput"
                        label="Search Treatment">
                        <Form.Control ref={searchQuery} type="search"/>
                    </FloatingLabel>
                    <Button onClick={() => setLoaded(false)}>
                    Search
                    </Button>
                </InputGroup>
            </Container>
            <Row>
                <h1 style={{textAlign: 'center'}}>Medications</h1>
            </Row>
            <Row style={{marginTop: 20, marginBottom: 20}}>
                <Col>
                </Col>
                <Col>
                    <Filters layout={layout}></Filters>
                </Col>
                
            </Row>
            <Row style={{marginTop: 20, marginBottom: 20}}>
                <Button style={{width: 100}} onClick={() => handleReset()} >Clear</Button>
            </Row>

            <Row>
                <Pagination className="justify-content-center">
                    {items}
                </Pagination>
            </Row>
            <Row>
                <p style={{textAlign: "center"}}>
                    Showing results  {Math.floor(data_length / numPages * (activePage - 1)) + 1} to {Math.floor(data_length / numPages * activePage) > data_length ? data_length : Math.floor(data_length / numPages * activePage)} of {data_length} 
                </p>
            </Row>

            <Row>
            {medicines.map((el) => {
                return (
                    <Col>
                        <MedicationCard key={el.id-1} name={el.name} inum={el.id-1} generic_name={el.generic_name}
                    manufacturer={el.manufacturer} form={el.form} route={el.route}/>
                    </Col>
                )
            })}
            </Row>
        </Container>
        </div>
        
    )
}

export default Medication