import React, {useState, useEffect} from 'react';
import Container from 'react-bootstrap/esm/Container';
import { useParams, Link } from "react-router-dom";
import symptom_data from '../data/symptoms_NHS.json';
import Card from 'react-bootstrap/Card';
import axios from 'axios';
const container_style = {
    display: 'flex'
}



const Symptom = () => {
    // params.id to grab id of a certain instance
    const client = axios.create({
        baseURL: 'https://api.symptomsolver.me'
    });

    const { id } = useParams();
    const [symptom, setSymptom] = useState([]);
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        const fetchSymptom = async () => {
            if (symptom === undefined || symptom.length === 0) {
                await client    
                    .get(`symptoms/${parseInt(id) + 1}`)
                    .then((response) => {
                        setSymptom(response.data)
                    })
                    .catch((error) => console.log(error))
                setLoaded(true)
            }
        };
        fetchSymptom();
    }, [symptom, loaded]);

    const name = symptom.name;
    const location = symptom.location;
    const rarity = symptom.rarity;
    const severity = symptom.severity;
    let hasRelatedLocation = symptom.related_loc_ids === undefined ? false : symptom.related_loc_ids.length > 0;
    let hasRelatedMedication = symptom.related_med_ids === undefined ? false : symptom.related_med_ids.length > 0;
    let loc_id;
    let med_id;

    const [relatedLocation, setRelatedLocation] = useState([]);
    const [relatedMedication, setRelatedMedication] = useState([]);

    if (hasRelatedLocation) {
        loc_id = symptom.related_loc_ids[0];
        const fetchRelatedLocation = async () => {
            if (relatedLocation === undefined || relatedLocation.length === 0) {
                await client    
                    .get(`locations/${loc_id}`)
                    .then((response) => {
                        setRelatedLocation(response.data)
                    })
                    .catch((error) => console.log(error))
                setLoaded(true)
            }
        };
        fetchRelatedLocation();
    }

    if (hasRelatedMedication) {
        med_id = symptom.related_med_ids[0];
        const fetchRelatedMedication = async () => {
            if (relatedMedication === undefined || relatedMedication.length === 0) {
                await client    
                    .get(`treatments/${med_id}`)
                    .then((response) => {
                        setRelatedMedication(response.data)
                    })
                    .catch((error) => console.log(error))
                setLoaded(true)
            }
        };
        fetchRelatedMedication();
    }


    

    return (
        <Container style={container_style}>
            <Card style={{width:'40rem', height: '40rem'}}>
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Text>Rarity: {rarity}</Card.Text>
                    <Card.Text>Severity: {severity}</Card.Text>
                    <Card.Text>Location: {location}</Card.Text>
                    <div>
                            { hasRelatedMedication? (<Link to={`/medications/${med_id-1}`}>Related Medication: {relatedMedication.name}</Link>) : (<p>Has no related medication</p>)}
                        </div>
                        <div>
                            { hasRelatedLocation? (<Link to={`/locations/${loc_id-1}`}>Related Location: {relatedLocation.name}</Link>) : (<p>Has no related location</p>) }
                        
                    </div>
                </Card.Body>
            </Card> 
            <iframe width="100%"
                        height="500"
                        referrerPolicy="no-referrer-when-downgrade"
                        src={"https://www.webmd.com/search/search_results/default.aspx?query="+name}
                        allowFullScreen></iframe>
        </Container>
    )
}

export default Symptom