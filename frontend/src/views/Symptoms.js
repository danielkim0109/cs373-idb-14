import React, { useState, useEffect, useRef } from "react";

import Container from 'react-bootstrap/esm/Container';
import {Row, Col, Form, FloatingLabel, InputGroup} from 'react-bootstrap'
import Pagination from "react-bootstrap/Pagination";
import Button from "react-bootstrap/Button";
import SymptomCard from '../components/SymptomCard';
import Filters from '../components/Filters';

import axios from "axios";


const client = axios.create({
    baseURL: "https://api.symptomsolver.me"
});
  

let queryRE = null;

const Symptoms = () => {
    const [symptoms, setSymptoms] = useState([]);
    
    
    const [loaded, setLoaded] = useState(false);
    const [activePage, setActivePage] = useState(1);
    const [data_length, setdata_length] = useState(122);
    
    const [sort, setSort] = useState("sort")
    const [severity, setSeverity] = useState("severity")
    const [rarity, setRarity] = useState("rarity")
    const [location, setLocation] = useState("location")

    const searchQuery = useRef("");

    function handleClick(page_num) {
        setActivePage(page_num);
        setLoaded(false);
    }

    const handleSortFilter = (value) => {
        setSort(value.toLowerCase());
        setLoaded(false);
    };

    const handleSeverityFilter = (value) => {
        setSeverity(value.toLowerCase());
        setLoaded(false);
    };
    
    const handleRarityFilter = (value) => {
        setRarity(value.toLowerCase());
        setLoaded(false);
    };
    
    const handleLocationFilter = (value) => {
        setLocation(value.toLowerCase());
        setLoaded(false);
    };

    function handleReset (){
        setSort("sort");
        setSeverity("severity");
        setRarity("rarity");
        setLocation("location");
        setLoaded(false);
    }
    
    const layout = [
        {
            label: "Sort",
            default: "none",
            options: ["ascending", "descending"],
            onclick: handleSortFilter
        },
        {
            label: "Severity",
            default: "none",
            options: ["1", "2", "3"],
            onclick: handleSeverityFilter
        },
        {
            label: "Rarity",
            default: "none",
            options: ["1", "2", "3", "4"],
            onclick: handleRarityFilter
        },
        {
            label: "Location",
            default: "none",
            options: ["Arms", "Back", "Bladder", "Blood", "Butt", "Chest", "Ears", "Eyes", "Face", "Feet", "FullBody",
                    "Genitals", "Hands", "Head", "Hips", "Legs", "Mouth", "Neck", "Nose", "Stomach"],
            onclick: handleLocationFilter
        }
    ]
    
    
    
    
    useEffect(()=>{
        const fetchSymptoms = async () => {
            if (!loaded) {
                let query=`symptoms?page=${activePage}`
                if (searchQuery.current.value != "") {
                    query = `/symptoms?search=${searchQuery.current.value}`;
                    queryRE = new RegExp(`(?:${searchQuery.current.value.replaceAll(" ", "|")})`,
                    "i"
                  );
                }
                if (sort !== "sort") {
                    query += `&sort=${sort}`;
                }
                if (severity !== "severity") {
                    query += `&severity=${severity}`;
                }
                if (rarity !== "rarity") {
                    query += `&rarity=${rarity}`;
                }
                if (location !== "location") {
                    query += `&location=${location}`;
                }
                console.log(query)
                await client
                  .get(query)
                  .then((response) => {
                    let numPages = response.data.length / 12;
                    let page_start = response.data.length / numPages * (activePage - 1);
                    let page_end = response.data.length / numPages * activePage;
                    console.log(response.data.length)
                    setSymptoms(response.data.slice(page_start, page_end));
                    setdata_length(response.data.length);
                  })
                  .catch((err) => console.log(err));
                setLoaded(true);
              }
        };
        fetchSymptoms();
    }, [symptoms, loaded]);
        
    
    let numPages = data_length/12;
    let items = [];
    for (let page_num = 1; page_num <= numPages + .99; page_num++) {
        items.push(
            <Pagination.Item
            key={page_num}
            onClick={() => handleClick(page_num)}
            active={page_num === activePage}
            >
            {page_num}
            </Pagination.Item>
        );
    }

    
    return (
        <div>
            
            <Container>
            <Container className="my-5">
                <InputGroup className="mb-3">
                    <FloatingLabel
                        controlId="searchInput"
                        label="Search Symptoms">
                        <Form.Control ref={searchQuery} type="search"/>
                    </FloatingLabel>
                    <Button onClick={() => setLoaded(false)}>
                    Search
                    </Button>
                </InputGroup>
            </Container>

            <Row>
                <h1 style={{textAlign: 'center'}}>Symptoms</h1>
            </Row>

            <Row style={{marginTop: 20, marginBottom: 20}}>
                <Col>
                </Col>
                <Col>
                    <Filters layout={layout}></Filters>
                </Col>
                
            </Row>
            <Row style={{marginTop: 20, marginBottom: 20}}>
                <Button style={{width: 100}} onClick={() => handleReset()}>Clear</Button>
            </Row>


            <Row>
                <Pagination className="justify-content-center">
                    {items}
                </Pagination>
            </Row>
            <Row>
                <p style={{textAlign: "center"}}>
                    Showing results  {Math.floor(data_length / numPages * (activePage - 1)) + 1} to {Math.floor(data_length / numPages * activePage) > data_length ? data_length : Math.floor(data_length / numPages * activePage)} of {data_length} 
                </p>
            </Row>

            <Row>
            {symptoms.map((el) =>{
                return (
                    <Col>
                        <SymptomCard key={el.id-1} name={el.name} severity={el.severity} rarity={el.rarity} inum={el.id-1} location={el.location}/>
                    </Col>
                )
            }
            )}
            
            </Row>
        </Container>
        </div>
        
    )
}

export default Symptoms