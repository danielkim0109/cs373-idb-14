import React, { useState, useEffect, useRef } from "react";

import Container from 'react-bootstrap/esm/Container';
import {Row, Col, Form, FloatingLabel, InputGroup} from 'react-bootstrap'
import Pagination from "react-bootstrap/Pagination";
import Button from "react-bootstrap/Button";

import LocationCard from '../components/LocationCard';
import Filters from '../components/Filters';

import axios from 'axios';

const client = axios.create({
    baseURL: "https://api.symptomsolver.me",
});

let queryRE = null;

const Locations = () => {
    const [locations, setLocations] = useState([]);
    
    const [loaded, setLoaded] = useState(false);
    const [activePage, setActivePage] = useState(1);
    const [data_length, setdata_length] = useState(122);
    
    
    const [sort, setSort] = useState("sort")
    const [city, setCity] = useState("city")
    const [rating, setRating] = useState("rating")
    const [reviews, setReviews] = useState("reviews")

    const searchQuery = useRef("");

    function handleClick(page_num) {
        setActivePage(page_num);
        setLoaded(false);
    }

    const handleSortFilter = (value) => {
        setSort(value.toLowerCase());
        setLoaded(false);
    };

    const handleCityFilter = (value) => {
        setCity(value.toLowerCase());
        setLoaded(false);
    };
    
    const handleRatingFilter = (value) => {
        setRating(value.toLowerCase());
        setLoaded(false);
    };
    
    const handleReviewsFilter = (value) => {
        setReviews(value);
        setLoaded(false);
    };

    function handleLocationReset() {
        setSort("sort");
        setCity("city");
        setRating("rating");
        setReviews("reviews");
        setLoaded(false);

    };
    
    const layout = [
        {
            label: "Sort",
            default: "none",
            options: ["ascending", "descending"],
            onclick: handleSortFilter
        },
        {
            label: "City",
            default: "none",
            options: ["New York", "Los Angeles", "Chicago", "Houston", "Philadelphia",
            "Phoenix", "San Antonio", "San Diego", "Dallas", "San Jose", "Austin",
            "Jacksonville", "San Francisco", "Indianapolis ", "Columbus", "Fort Worth",
            "Charlotte", "Seattle", "Denver", "El Paso" ],
            onclick: handleCityFilter
        },
        {
            label: "Rating",
            default: "none",
            options: ["1", "2", "3", "4", "5"],
            onclick: handleRatingFilter
        },
        {
            label: "Reviews",
            default: "none",
            options: [10, 100, 1000],
            onclick: handleReviewsFilter
        }
    ]

    useEffect(()=>{
        const fetchLocations = async () => {
            if (!loaded) {
                let query=`locations?page=${activePage}`
                if (searchQuery.current.value != "") {
                    query = `/locations?search=${searchQuery.current.value}`;
                    queryRE = new RegExp(`(?:${searchQuery.current.value.replaceAll(" ", "|")})`,
                    "i"
                  );
                }
                if (sort !== "sort") {
                    query += `&sort=${sort}`;
                }
                if (city !== "city") {
                    query += `&city=${city}`;
                }
                if (rating !== "rating") {
                    query += `&rating=${rating}`;
                }
                if (reviews !== "reviews") {
                    query += `&reviews=${reviews}`;
                }
                console.log(query)
                await client
                  .get(query)
                  .then((response) => {
                    let numPages = response.data.length / 20;
                    let page_start = response.data.length / numPages * (activePage - 1);
                    let page_end = response.data.length / numPages * activePage;
                    console.log(response.data.length)
                    setLocations(response.data.slice(page_start, page_end));
                    setdata_length(response.data.length);
                  })
                  .catch((err) => console.log(err));
                setLoaded(true);
              }
        };
        fetchLocations();
    }, [loaded, activePage])

    let numPages = data_length / 20;
    let items = [];
    for (let page_num = 1; page_num <= numPages + .99; page_num++) {
        items.push(
            <Pagination.Item
            key={page_num}
            onClick={() => handleClick(page_num)}
            active={page_num === activePage}
            >
            {page_num}
            </Pagination.Item>
        );
    }
    return (
        <div>
            <Container>
            <Container className="my-5">
                <InputGroup className="mb-3">
                    <FloatingLabel
                        controlId="searchInput"
                        label="Search Locations">
                        <Form.Control ref={searchQuery} type="search"/>
                    </FloatingLabel>
                    <Button onClick={() => setLoaded(false)}>
                    Search
                    </Button>
                </InputGroup>
            </Container>
            <Row>
                <h1 style={{textAlign: 'center'}}>Locations</h1>
            </Row>

            <Row style={{marginTop: 20, marginBottom: 20}}>
                <Col>
                </Col>
                <Col>
                    <Filters layout={layout}></Filters>
                </Col>
                
            </Row>
            <Row style={{marginTop: 20, marginBottom: 20}}>
                <Button style={{width: 100}} onClick = {() => handleLocationReset()}>Clear</Button>
            </Row>
            <Row>
                <Pagination className="justify-content-center">
                    {items}
                </Pagination>
            </Row>
            <Row>
                <p style={{textAlign: "center"}}>
                    Showing results  {Math.floor(data_length / numPages * (activePage - 1)) + 1} to {Math.floor(data_length / numPages * activePage) > data_length ? data_length : Math.floor(data_length / numPages * activePage)} of {data_length} 
                </p>
            </Row>

            <Row>
                {locations.map((el) => {
                    return (
                        <Col>
                            <LocationCard key={el.id-1} name={el.name} rating={el.rating} reviews={el.reviews} inum={el.id-1} image={el.image} address={el.address} type={el.location_type}/>
                        </Col>
                    )
                })}
            </Row>
        </Container>
        </div>
    )
}

export default Locations