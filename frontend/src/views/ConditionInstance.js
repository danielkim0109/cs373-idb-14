import React from 'react';
import Container from 'react-bootstrap/Container';
import Stack from 'react-bootstrap/Stack';
import Card from 'react-bootstrap/Card';
import { useParams } from 'react-router-dom';
import condition_data from '../data/conditions_NHS.json';

import '../styles/Views.css'


const container_style = {
    display: 'flex'
}



const ConditionInstance = () => {
    // params.id to grab id of a certain instance
    const { id } = useParams();
    const condition = condition_data[id];

    return (
        <Stack className='background'>
            <Container style={container_style}>
                <Card style={{width:'40rem', height: '40rem'}}>
                    <Card.Body>
                        <Card.Title>Condition: {condition.name}</Card.Title>
                        <Card.Img src={condition.image} style={{height: '30vh', width: '30vh'}}/>
                        <Card.Text>Number of Cases: {condition.number_of_cases}</Card.Text>
                        <Card.Text>Type of Condition: {condition.type_of_condition}</Card.Text>
                        <Card.Text>Contagious: {condition.contagious}</Card.Text>
                        <Card.Text>Seasonal: {condition.seasonal}</Card.Text>

                        
                        <Card.Text>Types of Symptoms:</Card.Text>
                        <Card.Text>Possible Treatment</Card.Text>
                        
        
                    </Card.Body>
                </Card>
            </Container>
            <Container>
            <iframe width="100%"
                            height="200"
                            referrerPolicy="no-referrer-when-downgrade"
                            src={"https://www.webmd.com/search/search_results/default.aspx?query="+condition.name}
                            allowFullScreen></iframe>
            </Container>
        </Stack>
    )
}

export default ConditionInstance