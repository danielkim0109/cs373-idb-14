import React, { useState, useEffect} from "react";
import Container from 'react-bootstrap/Container';
import Card from 'react-bootstrap/Card';
import { useParams, Link } from 'react-router-dom';
import axios from "axios"

const container_style = {
    display: 'flex',
}



const Location = () => {

    const client = axios.create({
        baseURL: 'https://api.symptomsolver.me'
    });


    const { id } = useParams();
    const [locations, setLocation] = useState([]);
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        const fetchLocations = async () => {
            if (locations === undefined || locations.length === 0) {
                await client    
                    .get(`locations/${parseInt(id) + 1}`)
                    .then((response) => {
                        setLocation(response.data)
                    })
                    .catch((error) => console.log(error))
                setLoaded(true)

            }
        };
        fetchLocations();
    }, [locations, loaded]);

    const name = locations.name;
    const image = locations.image;
    const address = locations.address;
    const type = locations.type;
    const rating = locations.rating;
    const reviews = locations.reviews;

    let hasRelatedSymptom = locations.related_symp_ids === undefined ? false : locations.related_symp_ids.length > 0;
    let hasRelatedMedication = locations.related_med_ids === undefined ? false : locations.related_med_ids.length > 0;
    let symp_id;
    let med_id;

    const [relatedSymptom, setRelatedSymptom] = useState([]);
    const [relatedMedication, setRelatedMedication] = useState([]);

    if (hasRelatedSymptom) {
        symp_id = locations.related_symp_ids;
        const fetchRelatedSymptom = async () => {
            if (relatedSymptom === undefined || relatedSymptom.length === 0) {
                await client    
                    .get(`symptoms/${symp_id}`)
                    .then((response) => {
                        setRelatedSymptom(response.data)
                    })
                    .catch((error) => console.log(error))
                setLoaded(true)
            }
        };
        fetchRelatedSymptom();
    }

    if (hasRelatedMedication) {
        med_id = locations.related_med_ids;
        const fetchRelatedMedication = async () => {
            if (relatedMedication === undefined || relatedMedication.length === 0) {
                await client    
                    .get(`treatments/${med_id}`)
                    .then((response) => {
                        setRelatedMedication(response.data)
                    })
                    .catch((error) => console.log(error))
                setLoaded(true)
            }
        };
        fetchRelatedMedication();
    }



    return (
        <Container style={container_style}>
            <Card style={{width:'40rem', height: '60rem'}}>
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Img src={image}/>
                    <Card.Text>Address: {address}</Card.Text>
                    <Card.Text>Location Type: {type}</Card.Text>
                    <Card.Text>Rating: {rating}</Card.Text>
                    <Card.Text>Number of Reviews: {reviews}</Card.Text>
                    
                    <iframe
                        width="100%"
                        height="300"
                        referrerPolicy="no-referrer-when-downgrade"
                        src={"https://www.google.com/maps/embed/v1/place?key=AIzaSyCnCyEh87auJpydaJxbABPYJiwxj51aBSc&q=" + name}
                        allowFullScreen></iframe>
                    <div>
                            { hasRelatedMedication ? (<Link to={`/medications/${med_id-1}`}>Related Medication: {relatedMedication.name}</Link>) : (<p>Has no related medication</p>)}
                        </div>
                        <div>
                            { hasRelatedSymptom ? (<Link to={`/symptoms/${symp_id-1}`}>Related Symptom: {relatedSymptom.name}</Link>) : (<p>Has no related symptom</p>) }
                        
                    </div>
                </Card.Body>
            </Card>  
        </Container>
    )
}

export default Location

/* Used Code:

https://gitlab.com/jaegarciah/cs373-idb-19/-/blob/main/front-end/src/pages/CityDetail.tsx

Used for inserting google maps into instance. 

*/