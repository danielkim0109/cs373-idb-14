import React from 'react';
import ConditionCard from '../components/ConditionCard.js';
import conditions from '../data/conditions_NHS.json';
import {Container, Stack, Row, Col}from 'react-bootstrap';
import '../styles/Views.css'


const container_style = {
    display: 'flex'
}

const Conditions = () => {


    


    return (
        <Stack className ='background'>
            <Container style={container_style}>
            <Row>
                {conditions.map((el, i) => {
                    return (
                        <Col>
                            <ConditionCard key={i} name={el.name} desc={el.description} inum={i} 
                            cases={el.number_of_cases} type={el.type_of_condition} contagious={el.contagious}/>
                        </Col>
                    )
                })}
            </Row>
            </Container>
        </Stack>
        
    )
}

export default Conditions