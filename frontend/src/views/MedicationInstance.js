import React, {useState, useEffect} from 'react';
import Container from 'react-bootstrap/esm/Container';
import { useParams, Link } from "react-router-dom";
import Card from 'react-bootstrap/Card';
import axios from "axios"

const container_style = {
    display: 'flex'
}



const Medication = () => {
    // params.id to grab id of a certain instance 
    
    const client = axios.create({
        baseURL: 'https://api.symptomsolver.me'
    });


    const { id } = useParams();
    const [medication, setMedication] = useState([]);
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        const fetchMedication = async () => {
            if (medication === undefined || medication.length === 0) {
                await client    
                    .get(`treatments/${parseInt(id) + 1}`)
                    .then((response) => {
                        setMedication(response.data)
                    })
                    .catch((error) => console.log(error))
                setLoaded(true)
            }
        };
        fetchMedication();
    }, [medication, loaded]);

    const brand_name = medication.name;
    const image = medication.image;
    const generic_name = medication.generic_name;
    const manufacturer = medication.manufacturer;
    const form = medication.form;
    const route = medication.route;
    let hasRelatedLocation = medication.related_loc_ids === undefined ? false : medication.related_loc_ids.length > 0;
    let hasRelatedSymptom = medication.related_symp_ids === undefined ? false : medication.related_symp_ids.length > 0;
    let loc_id;
    let symp_id;

    const [relatedLocation, setRelatedLocation] = useState([]);
    const [relatedSymptom, setRelatedSymptom] = useState([]);

    if (hasRelatedLocation) {
        loc_id = medication.related_loc_ids[0];
        const fetchRelatedLocation = async () => {
            if (relatedLocation === undefined || relatedLocation.length === 0) {
                await client    
                    .get(`locations/${loc_id}`)
                    .then((response) => {
                        setRelatedLocation(response.data)
                    })
                    .catch((error) => console.log(error))
                setLoaded(true)
            }
        };
        fetchRelatedLocation();
    }

    if (hasRelatedSymptom) {
        symp_id = medication.related_symp_ids[0];
        const fetchRelatedSymptom = async () => {
            if (relatedSymptom === undefined || relatedSymptom.length === 0) {
                await client    
                    .get(`symptoms/${symp_id}`)
                    .then((response) => {
                        setRelatedSymptom(response.data)
                    })
                    .catch((error) => console.log(error))
                setLoaded(true)
            }
        };
        fetchRelatedSymptom();
    }

    
    return (
        <Container style={container_style}>
            <Card style={{width:'40rem', height: '40rem'}}>
                <Card.Body>
                    <Card.Title>{brand_name}</Card.Title>
                    <Card.Img src={image} style={{height: '30vh', width: '30vh'}}/>
                    <Card.Text>Generic Name: {generic_name}</Card.Text>
                    <Card.Text>Manufacturer: {manufacturer}</Card.Text>
                    <Card.Text>Form: {form}</Card.Text>
                    <Card.Text>Route: {route}</Card.Text>
                    <div>
                            { hasRelatedSymptom ? (<Link to={`/symptoms/${symp_id-1}`}>Related Symptom: {relatedSymptom.name}</Link>) : (<p>Has no related symptom</p>)}
                        </div>
                        <div>
                            { hasRelatedLocation ? (<Link to={`/locations/${loc_id-1}`}>Related Location: {relatedLocation.name}</Link>) : (<p>Has no related location</p>) }
                        
                    </div>
                </Card.Body>
            </Card>  
            <iframe width="100%"
                        height="500"
                        referrerPolicy="no-referrer-when-downgrade"
                        src={"https://www.webmd.com/search/search_results/default.aspx?query="+brand_name}
                        allowFullScreen></iframe>
        </Container>
    )
}

export default Medication