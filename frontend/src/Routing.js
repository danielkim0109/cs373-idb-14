import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Home from './views/Home.js';
import About from './views/About.js';
import Symptoms from './views/Symptoms';
import Locations from './views/Locations';
import Medication from './views/Medication';
import Location from './views/LocationInstance.js';
import MedicationInstance from './views/MedicationInstance';
import Symptom from './views/SymptomInstance.js';
import Visualizations from './views/Visualizations.js';

const Router = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path = '/' element={<Home />} />
                <Route path = '/about' element={<About />} />

                <Route path = '/symptoms' element={<Symptoms />}/>
                <Route path = '/symptoms/:id' element={<Symptom />}/>

                <Route path = '/locations' element={<Locations />}/>
                <Route path = '/locations/:id' element={<Location />}/>
                
                <Route path = '/medications' element={<Medication />}/>
                <Route path = '/medications/:id' element={<MedicationInstance />}/>

                <Route path = '/visualizations' element={<Visualizations />} />
            </Routes>
        </BrowserRouter>
    )
}

export default Router