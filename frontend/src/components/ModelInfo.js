const models = [
{
    title: "Symptoms",
    link: "/symptoms"
},

{
    title: "Medications",
    link: "/medications"
},

{
    title: "Locations",
    link: "/locations"
},
];

export { models };