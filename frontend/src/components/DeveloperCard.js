import React from 'react'
import Card from 'react-bootstrap/Card'

const DeveloperCard = (props) => {
    const {
        name,
        role,
        image,
        gituser,
        email,
        body,
        commits,
        issues,
        unitTests
    } = props.memberInfo;
    return (
        <Card>
            <Card.Body>
                <Card.Img src = {image} style={{ borderRadius: 1000}} />
                <Card.Title style={{paddingTop: 10}}>{name}</Card.Title>
                <Card.Subtitle>{role}</Card.Subtitle>
                <Card.Text>@{gituser}</Card.Text>
                <Card.Text>{body}</Card.Text>
            </Card.Body>
            <Card.Footer>commits: {commits}<br/>
                            issues: {issues}<br/>
                            unit tests: {unitTests}</Card.Footer>
        </Card>
    )
}
export default DeveloperCard;