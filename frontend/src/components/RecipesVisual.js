import React from "react";

import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { PieChart, Pie, Tooltip, ResponsiveContainer } from 'recharts';

import recipiesdata from '../data/recipies.json';



const RecipesVisual = () => {
    let data = recipiesdata;
    let temp_data = []
    for (let id = 0; id < data.length; id++) {
        let recipeTypes = data[id]["dish_types"]
        for (let d = 0; d < recipeTypes.length; d++){
            let found = false;
            let i = 0;
            while(found !== true && i < temp_data.length){
                if(temp_data[i]["name"] === recipeTypes[d]){
                    temp_data[i]["count"]++;
                    found = true;
                }
                i++;
            }
            if(found !== true){
                temp_data.push({"name": recipeTypes[d], "count": 1})
            }
        }
        
    }
    const recipes = temp_data

    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Recipes Grouped by Dish Type</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <PieChart width={400} height={400}>
                            <Pie
                                nameKey = "name"
                                dataKey= "count"
                                isAnimationActive={false}
                                data={recipes}
                                cx="50%"
                                cy="50%"
                                outerRadius={200}
                                fill="#FF5733 "
                                label
                            />
                            <Tooltip />
                        </PieChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    );

}
export default RecipesVisual;