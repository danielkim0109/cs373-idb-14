import React from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import '../styles/NavBar.css';

const SiteNavbar = () => {
    return (
        <Navbar bg="light" sticky="top" expand="lg" class="color-nav">
            <Container class="d-flex justify-content-center">
                <Navbar.Brand href ="/">SymptomSolver</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse>
                    <Nav>
                        <Nav.Link href="/">Home</Nav.Link>
                        <Nav.Link href="/about/">About</Nav.Link>
                        <Nav.Link href="/symptoms/">Symptoms</Nav.Link>
                        <Nav.Link href="/medications/">Medications</Nav.Link>
                        <Nav.Link href="/locations/">Locations</Nav.Link>
                        <Nav.Link href="/visualizations/">Visualizations</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )
}

export default SiteNavbar