const tools = [
{
    title: "Visual Studio Code",
    description: "IDE to edit code",
    link: "https://code.visualstudio.com/"
},

{
    title: "NameCheap",
    description: "Used to obtain Domain name",
    link: "https://www.namecheap.com/"
},

{
    title: "GitLab",
    description: "Stores project code and platform for continuous integration",
    link: "https://gitlab.com/"
},

{
    title: "AWS Amplify",
    description: "Hosts the website",
    link: "https://aws.amazon.com/amplify/?trk=66d9071f-eec2-471d-9fc0-c374dbda114d&sc_channel=ps&s_kwcid=AL!4422!3!646025317188!e!!g!!aws%20amplify&ef_id=Cj0KCQiAxbefBhDfARIsAL4XLRqZspS_C88ynIEA71yrFovre3gMQrWrOOQoehDlVONNZ5n5DSXv2HUaAkFGEALw_wcB:G:s&s_kwcid=AL!4422!3!646025317188!e!!g!!aws%20amplify"
},

{
    title: "Postman",
    description: "Builds Apis",
    link: "https://www.postman.com/"
},

{
    title: "React",
    description: "A library used to create interactive apps",
    link: "https://reactjs.org/"
},

{
    title: "React Bootstrap",
    description: "CSS framework which works with React",
    link: "https://react-bootstrap.github.io/"
},

{
    title: "Microsoft Teams",
    description: "Communication platform for team members",
    link: "https://www.microsoft.com/en-us/microsoft-teams/group-chat-software"
}

];
const apis = [
{
    title: "Google Maps API",
    description: "This API is used to collect map information on locations",
    link: "https://developers.google.com/maps "
},

{
    title: "Google Cloud API",
    description: "This API is used to collect information about healthcare locations",
    link: "https://cloud.google.com/healthcare-api "
},

{
    title: "NHS API",
    description: "This API is used to collect information about illnesses and symptoms",
    link: "https://www.nhs.uk/conditions/"
},

{
    title: "CDC API",
    description: "This API is used to collect information about illnesses and symptoms",
    link: "https://open.cdc.gov/apis.html"
},

];

export { tools, apis };