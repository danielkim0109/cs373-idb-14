import React from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container'
import '../styles/Card.css';

const container_style = {
    width: '100%',
    marginTop: 20
}

const card_style = {
    width: '20rem'
}

const SymptomCard = (props) => {
    return (
        <Container style={container_style}>
            <Card style={card_style}>
                <Card.Body>
                    <Card.Title>{props.name}</Card.Title>
                    <Card.Text>Severity: {props.severity}</Card.Text>
                    <Card.Text>Rarity: {props.rarity}</Card.Text>
                    <Card.Text>Location: {props.location}</Card.Text>
                    <Button variant="primary" href={"/symptoms/" + props.inum}>Learn More</Button>
                </Card.Body>
            </Card>
        </Container>
        
    )
}

export default SymptomCard
