import React from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/esm/Container';
import '../styles/Card.css';

const container_style = {
    width: '100%',
    marginTop: 20
}

const card_style = {
    width: '20rem'
}

const MedicationCard = (props) => {
    return (
        <Container style={container_style}>
            <Card style={card_style}>
                <Card.Img/>
                <Card.Body>
                <Card.Title>{props.name}</Card.Title>
                <Card.Text>Generic Name: {props.generic_name}</Card.Text>
                <Card.Text>Manufacturer: {props.manufacturer}</Card.Text>
                <Card.Text>Form: {props.form}</Card.Text>
                <Card.Text>Route: {props.route}</Card.Text>
                <Button variant="primary" href={"/medications/" + props.inum}>Learn More</Button>
                </Card.Body>
            </Card>
        </Container>
    )
}

export default MedicationCard