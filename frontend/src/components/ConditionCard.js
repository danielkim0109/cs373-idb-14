import React from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container'
import '../styles/Card.css';

const container_style = {
    width: '100%',
    marginTop: 20
}

const card_style = {
    width: '20rem'
}

const ConditionCard = (props) => {
    return (
        <Container style={container_style}>
            <Card style={card_style}>
                <Card.Body>
                    <Card.Title>{props.name}</Card.Title>
                    <Card.Text>Number of Cases: {props.cases}</Card.Text>
                    <Card.Text>Type of Condition: {props.type}</Card.Text>
                    <Card.Text>Contagious: {props.contagious}</Card.Text>
                    <Card.Text>{props.desc}</Card.Text>
                    <Button variant="primary" href={"/conditions/" + props.inum}>Learn More</Button>
                </Card.Body>
            </Card>
        </Container>
        
    )
}

export default ConditionCard
