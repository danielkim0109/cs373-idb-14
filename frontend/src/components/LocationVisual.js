import React, { useState, useEffect} from "react";

import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {ScatterChart, Scatter, XAxis, YAxis, CartesianGrid, Tooltip, ResponsiveContainer} from 'recharts';
import axios from "axios"




const LocationVisual = () => {
    const client = axios.create({
        baseURL: 'https://api.symptomsolver.me'
    });
    const [locations, setLocations] = useState([]);
    const [loaded, setLoaded] = useState(false);


    useEffect(()=>{
        const fetchLocations = async () => {
            if (!loaded) {
                let query=`locations`
                console.log(query)
                await client
                  .get(query)
                  .then((response) => {
                    setLocations(response.data);
                  })
                  .catch((err) => console.log(err));
                setLoaded(true);
              }
        };
        fetchLocations();
    }, [loaded])
    
    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Healthcare Locations: Reviews vs Ratings</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <ScatterChart
                            width={400}
                            height={400}
                            margin={{
                                top: 20,
                                right: 20,
                                bottom: 20,
                                left: 20,
                            }}
                        >
                            <CartesianGrid />
                            <XAxis type="number" dataKey="reviews" name="Number of Reviews" />
                            <YAxis type="number" dataKey="rating" name="Rating in Stars" domain={[0, 5]} ticks={[0,1,2,3,4,5]} />
                            <Tooltip cursor={{ strokeDasharray: '3 3' }} />
                            <Scatter name="Hospitals" data={locations} fill="#FF5733" />
                        </ScatterChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    );

  
}
export default LocationVisual;