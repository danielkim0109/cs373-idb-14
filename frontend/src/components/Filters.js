import React from 'react';
import Dropdown from 'react-bootstrap/Dropdown';
import {Container, Row, Col} from "react-bootstrap";
import DropdownMenu from 'react-bootstrap/esm/DropdownMenu';

const Filters = (props) => {
    return (
    <Container>
        <Row>
            {Object.entries(props.layout).map((entry) => {
                let val = entry[1]
                return(
                    <Col>
                    <Dropdown>
                        <Dropdown.Toggle variant="primary">
                            {val['label']}
                        </Dropdown.Toggle>
                        <DropdownMenu>
                        {val['options'].map(
                        (option) => (
                        <Dropdown.Item eventKey={option} active={option===val}  onClick = {() => val['onclick'](option)}>
                            {option}
                        </Dropdown.Item>
                    ),
                )}
                        </DropdownMenu>
                    </Dropdown>
                    </Col>
                )})}
        </Row>

    </Container>
    
    
    );
}

export default Filters
/*
export default function Filters(props) {
    return (<Container>
    <Row>
    {Object.entries(props.LAYOUT).map((entry) => {
        let key = entry[0];
        let layout = entry[1];
        let val = props.filters[key] ? props.filters[key] : layout['default'];
        return (
        <Col key={key}>
        <h6>{layout['label']}</h6>
        <Dropdown>
            <Dropdown.Toggle variant="dark" id={key}>
                {val}
            </Dropdown.Toggle>
            <Dropdown.Menu variant="dark">
                <Dropdown.Item eventKey="default" active={layout['default']==val} onClick={() => props.setFilters(prev => ({...prev, [key]: null}))}>
                    {layout['default']}
                </Dropdown.Item>
                <Dropdown.Divider />
                {layout['options'].map(
                    (option) => (
                        <Dropdown.Item eventKey={option} active={option==val} key={option} 
                        onClick={() => props.setFilters(prev => ({...prev, [key]: option}))}>
                            {option}
                        </Dropdown.Item>
                    ),
                )}
            </Dropdown.Menu>
        </Dropdown>
        </Col>
        );
    })}
    </Row>
    </Container>);
}

*/