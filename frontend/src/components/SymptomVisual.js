import React, { useState, useEffect} from "react";

import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, ReferenceLine, ResponsiveContainer } from 'recharts';
import axios from "axios"





const SymptomVisual = () => {
    const client = axios.create({
        baseURL: 'https://api.symptomsolver.me'
    });
    const [symptoms, setSymptoms] = useState([]);
    const [loaded, setLoaded] = useState(false);
    let data=[];


    useEffect(()=>{
        const fetchSymptoms = async () => {
            if (!loaded) {
                let query=`symptoms`
                await client
                  .get(query)
                  .then((response) => {
                    const temp_data = response.data
                    data=[];
                    for (let id = 0; id < temp_data.length; id++) {
                        let location = temp_data[id]["location"]
                        let severity = parseInt(temp_data[id]["severity"])
                        let found = false;
                        let i = 0;
                        while(found !== true && i < data.length){
                            if(data[i]["name"] === location){
                                data[i]["avg_sever"] = (data[i]["avg_sever"] * data[i]["count"] + severity) / (data[i]["count"] + 1)
                                data[i]["count"]++;
                                found = true;
                            }
                            i++;
                        }
                        if(found !== true){
                            data.push({"name": location, "count": 1, "avg_sever": severity})
                        }
                    }
                    console.log(data)
                    setSymptoms(data);
                  })
                  .catch((err) => console.log(err));
                  setLoaded(true);   
                
              }
        };
        fetchSymptoms();
    }, [loaded])
    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Average Severity of Diseases Based on Body Location</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <BarChart
                            width={500}
                            height={300}
                            data={symptoms}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="name" />
                            <YAxis/>
                            <ReferenceLine y={52919.29} stroke="#000" />
                            <Tooltip />
                            <Bar dataKey="avg_sever" fill="#FF5733" />
                        </BarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    );


  
}
export default SymptomVisual;