import React from 'react'
import Card from 'react-bootstrap/Card'

const ToolsCard = (props) => {
    const {
        title,
        description,
        link,
    } = props.toolInfo;
    return (
        <Card>
            <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Text>{description}</Card.Text>
            </Card.Body>
            <Card.Footer><a href={link}> Learn more</a></Card.Footer>
        </Card>
    )
}
export default ToolsCard;