import HussainPhoto from '../profilepics/HussainMurtaza.jpg';
import AndrewPhoto from '../profilepics/AndrewJaso.png';
import KaushikPhoto from '../profilepics/KaushikJampala.png';
import DanielPhoto from '../profilepics/DanielKim.png';
import PhucPhoto from '../profilepics/PhucDang.jpg';


const groupInfo = [
    {
        name: "Hussain Murtaza",
        role: "Front-End Developer",
        image: HussainPhoto,
        gituser: "hmurtaza5191",
        email: "hmurtaza5191@gmail.com",
        body: "I am a senior at UT majoring in Computer Science. During my free time I like to watch soccer and play basketball. ",
        commits: 0,
        issues: 0,
        unitTests: 0
    }, 
   {
        name: "Phuc Q Dang",
        role: "Back-End Developer",
        image: PhucPhoto,
        gituser: "peaetchyousee",
        email: "peaetchyousee@gmail.com",
        body: "I'm a junior in CS. On my free time, I like to play videogames, practice my violin, and occasionally just sit down and listen to music.",
        commits: 0,
        issues: 0,
        unitTests: 0
    },
   {
        name: "Kaushik Jampala",
        role: "Back-End Developer",
        image: KaushikPhoto,
        gituser: "kjampala",
        email: "kjampala@gmail.com",
        body: "I'm currently a senior computer science major at UT. During my free time I like to play videogames, run, and hang out with my friends.",
        commits: 0,
        issues: 0,
        unitTests: 0
    },
   {
        name: "Andrew Jaso",
        role: "Back-End Developer",
        image: AndrewPhoto,
        gituser: "AndrewJaso",
        email: "ajaso54@yahoo.com",
        body: "I am a Junior majoring in Computer Science at UT and I enjoy playing video games and doing karaoke",
        commits: 0,
        issues: 0,
        unitTests: 0
    },
   {
        name: "Daniel Kim",
        role: "Front-End Developer",
        image: DanielPhoto,
        gituser: "danielkim0109",
        email: "danielkim0109@gmail.com",
        body: "I'm a Senior at UT! I like reading, playing frisbee, and eating good food!",
        commits: 0,
        issues: 0,
        unitTests: 0
    }
];
export {groupInfo}