import React from 'react'
import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'

import '../styles/Card.css'

const ModelsCard = (props) => {
    const {
        title,
        link,
    } = props.modelInfo;
    return (
        <Card>
            <Card.Body className='card-body'> <Card.Title className='card-title'>{title}</Card.Title></Card.Body>
            <Card.Footer><Button href={link}> Learn more</Button></Card.Footer>
        </Card>
    )
}
export default ModelsCard;