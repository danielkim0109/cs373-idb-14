import React from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/esm/Container';
import '../styles/Card.css';

const container_style = {
    width: '100%',
    marginTop: 20
}

const card_style = {
    width: '20rem'
}

const LocationCard = (props) => {
    return (
        <Container style={container_style}>
            <Card style={card_style}>
                
                <Card.Body>
                    <Card.Title>{props.name}</Card.Title>
                    <Card.Text>Address: {props.address}</Card.Text>
                    <Card.Text>Location Type: {props.type}</Card.Text>
                    <Card.Text>Rating {props.rating}</Card.Text>
                    <Card.Text>Number of Reviews {props.reviews}</Card.Text>
                    <Button variant="primary" href={"/locations/" + props.inum}>Learn More</Button>
                </Card.Body>
            </Card>
        </Container>
    )
}

export default LocationCard


// HCA Houston Healthcare Medical Center, Houston Methodist Hospital,
// Kindred Hospital Houston Medical Center
// <Card.Img src={props.image} style={{height: 200, objectFit: 'cover'}}/>
