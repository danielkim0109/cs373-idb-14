import React, { useState, useEffect} from "react";

import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { PieChart, Pie, Tooltip, ResponsiveContainer } from 'recharts';
import axios from "axios"


const MedicationsVisual = () => {
    const client = axios.create({
        baseURL: 'https://api.symptomsolver.me'
    });
    const [medications, setMedications] = useState([]);
    const [loaded, setLoaded] = useState(false);
    let data=[];


    useEffect(()=>{
        const fetchMedications = async () => {
            if (!loaded) {
                let query=`treatments`
                await client
                  .get(query)
                  .then((response) => {
                    const temp_data = response.data
                    data=[];
                    for (let id = 0; id < temp_data.length; id++) {
                        let route = temp_data[id]["route"]
                        let found = false;
                        let i = 0;
                        while(found !== true && i < data.length){
                            if(data[i]["name"] === route){
                                data[i]["count"]++;
                                found = true;
                            }
                            i++;
                        }
                        if(found !== true){
                            data.push({"name": route, "count": 1})
                        }
                    }
                    console.log(data)
                    setMedications(data);
                  })
                  .catch((err) => console.log(err));
                  setLoaded(true);   
                
              }
        };
        fetchMedications();
    }, [loaded])

    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Medications Grouped by Route</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <PieChart width={400} height={400}>
                            <Pie
                                nameKey = "name"
                                dataKey= "count"
                                isAnimationActive={false}
                                data={medications}
                                cx="50%"
                                cy="50%"
                                outerRadius={200}
                                fill="#FF5733"
                                label
                            />
                            <Tooltip />
                        </PieChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    );

}
export default MedicationsVisual;