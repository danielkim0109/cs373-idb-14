import React from "react";

import Container from "react-bootstrap/Container";
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, ReferenceLine, ResponsiveContainer } from 'recharts';

import restaurantsdata from '../data/restaurants.json';




const RestaurantVisual = () => {
    let data = restaurantsdata;
    let temp_data = []
    for (let id = 0; id < data.length; id++) {
        let foodTypes = data[id]["food_types"].split(", ")
        let rating = parseFloat(data[id]["rating"])
        for (let d = 0; d < foodTypes.length; d++){
            let found = false;
            let i = 0;
            while(found !== true && i < temp_data.length){
                if(temp_data[i]["name"] === foodTypes[d]){
                    temp_data[i]["avg_rating"] = (temp_data[i]["avg_rating"] * temp_data[i]["count"] + rating) / (temp_data[i]["count"] + 1)
                    temp_data[i]["count"]++;
                    found = true;
                }
                i++;
            }
            if(found !== true){
                temp_data.push({"name": foodTypes[d], "count": 1, "avg_rating": rating})
            }
        }
        
    }
    const restaurants = temp_data
    return (
        <Container fluid="md">
            <Row style={{ width: "100%", height: 600 }}>
                <h3 className="p-5 text-center">Average Rating for Each Restaurant Type</h3>
                <Col>
                    <ResponsiveContainer width="100%" height="100%">
                        <BarChart
                            width={500}
                            height={300}
                            data={restaurants}
                            margin={{
                                top: 5,
                                right: 30,
                                left: 20,
                                bottom: 5,
                            }}
                        >
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="name" />
                            <YAxis/>
                            <ReferenceLine y={52919.29} stroke="#000" />
                            <Tooltip />
                            <Bar dataKey="avg_rating" fill="#FF5733" />
                        </BarChart>
                    </ResponsiveContainer>
                </Col>
            </Row>
        </Container>
    );


  
}
export default RestaurantVisual;