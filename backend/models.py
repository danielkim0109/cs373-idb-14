from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import ARRAY, Integer

db = SQLAlchemy()


class Treatment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    generic_name = db.Column(db.String)
    manufacturer = db.Column(db.String)
    form = db.Column(db.String)
    route = db.Column(db.String)
    image = db.Column(db.String)
    related_loc_id = db.Column(ARRAY(Integer))
    related_symp_id = db.Column(ARRAY(Integer))

    def as_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "generic_name": self.generic_name,
            "manufacturer": self.manufacturer,
            "form": self.form,
            "route": self.route,
            "image": self.image,
            "related_loc_ids": self.related_loc_id,
            "related_symp_ids": self.related_symp_id,
        }


class Symptom(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    severity = db.Column(db.String)
    rarity = db.Column(db.String)
    location = db.Column(db.String)
    related_loc_id = db.Column(ARRAY(Integer))
    related_med_id = db.Column(ARRAY(Integer))

    def as_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "severity": self.severity,
            "rarity": self.rarity,
            "location": self.location,
            "related_loc_ids": self.related_loc_id,
            "related_med_ids": self.related_med_id,
        }


class Location(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    address = db.Column(db.String)
    rating = db.Column(db.Float)
    reviews = db.Column(db.Integer)
    image = db.Column(db.String)
    related_med_id = db.Column(ARRAY(Integer))
    related_symp_id = db.Column(ARRAY(Integer))

    def as_dict(self):
        return {
            "id": self.id,
            "name": self.name,
            "address": self.address,
            "rating": self.rating,
            "reviews": self.reviews,
            "image": self.image,
            "related_med_ids": self.related_med_id,
            "related_symp_ids": self.related_symp_id,
        }
