import requests
import json
from time import sleep
from pathlib import Path


def get_medicines():
    base_url = "https://api.fda.gov/drug/label.json"
    # base_url = "https://api.fda.gov/drug/ndc.json"
    query_params = {
        "count": "openfda.brand_name.exact"
    }
    YOUR_API_KEY = '98e0887af824418b9350aad3125d68ec'
    headers = {'Ocp-Apim-Subscription-Key': YOUR_API_KEY}
    response = requests.get(base_url, params=query_params)
    if response.status_code == 200:
        data = json.loads(response.content)
        medicines = []
        for item in data["results"]:
            #base_url2 = "https://api.fda.gov/drug/label.json"
            drug_name = item["term"] 
            base_url2 = "https://api.fda.gov/drug/ndc.json?search="+drug_name
            
            print(drug_name)
            
            response2 = requests.get(base_url2)
            if response2.status_code == 200:
                data2 = json.loads(response2.content)
                if (not data2["results"] or "manufacturer_name" not in data2["results"][0]["openfda"]):
                    continue
                print("pass")
                url2 = f'https://api.bing.microsoft.com/v7.0/images/search?q={drug_name}&count=1'
                response2 = requests.get(url2, headers=headers).json()
                image_url = response2['value'][0]['contentUrl']
                
                drug_info = {
                    "brand_name": data2["results"][0]["brand_name"] if "brand_name" in data2["results"][0] else drug_name, 
                    "generic_name": data2["results"][0]["generic_name"],
                    "manufacturer": data2["results"][0]["openfda"]["manufacturer_name"][0],
                    "form": data2["results"][0]["dosage_form"],
                    "route": data2["results"][0]["route"][0],
                    "image": image_url
                    
                }

                medicines.append(drug_info)
                
            else:
                print(f"Error: {response2.status_code} - {response2.reason}")
                
        json_object = json.dumps(medicines, indent=4)
        with open ("medicines2.json", "w") as outfile:
            outfile.write(json_object)

    else:
        print(f"Error: {response.status_code} - {response.reason}")


def get_locations():
   
    API_KEY = "AIzaSyCnCyEh87auJpydaJxbABPYJiwxj51aBSc"
    Cities = ["New York",
               " Los Angeles" ,
               " Chicago ",
                "Houston",
                "Philadelphia" ,
                "Phoenix" ,
               " San Antonio" ,
               " San Diego" ,
                "Dallas ",
               " San Jose",
                "Austin",
                "Jacksonville",
               " San Francisco",
                "Indianapolis ",
                "Columbus" ,
               " Fort Worth" ,
               " Charlotte" ,
                "Seattle",
                "Denver" ,
                "El Paso" ]
    hospitals = []

    for city in Cities:
        params = {
            "query": "hospitals in " + city,
            "key": API_KEY
        }
        response = requests.get("https://maps.googleapis.com/maps/api/place/textsearch/json", params=params)
        data = json.loads(response.text)
        #print(data["results"])
        YOUR_API_KEY = '98e0887af824418b9350aad3125d68ec'
        headers = {'Ocp-Apim-Subscription-Key': YOUR_API_KEY}
        


        for result in data["results"]:
            hospital_data = {}
            hospital_data["name"] = result["name"]
            hospital_data["address"] = result["formatted_address"]
            hospital_data["rating"] = result["rating"].__str__()
            hospital_data["reviews"] = result["user_ratings_total"].__str__()
            url2 = f'https://api.bing.microsoft.com/v7.0/images/search?q={hospital_data["name"]}&count=1'
            response2 = requests.get(url2, headers=headers).json()
            image_url = response2['value'][0]['contentUrl']
            hospital_data["image"] = image_url
            hospitals.append(hospital_data)
    json_object = json.dumps(hospitals, indent=4)
    with open ("hospitals.json", "w") as outfile:
        outfile.write(json_object)


def get_data():
    API_KEY: str = "630860ff49604e2285e0d61263d0c7c9"
    url = "https://api.nhs.uk/conditions"
    symptom_dict = {}
    condition_dict = {}
    other_dict = {}
    for i in range(0, 26):
        params = {
            "category": chr(65 + i)
        }
        print("Currently querying for all {} entries...".format(chr(65 + i)))
        result = requests.get(
            url, 
            params = params,
            headers = {"subscription-key": API_KEY}
            )
        
        entries = result.json()['significantLink']
        for entry in entries:
            genres = entry["mainEntityOfPage"]["genre"]
            if "Symptom" in genres:
                
                symptom_dict[entry["name"]] = entry
                
            # elif "Condition" in genres:
            #     condition_dict[entry["name"]] = entry
            # else:
            #     other_dict[entry["name"]] = entry
            
        sleep(7)
        
    # with open ('symptoms_NHS.json', 'w') as f:
    #     json.dump(symptom_dict, f)
    # json_object = json.dumps(symptom_dict, indent=4)
    # with open ("symptoms_NHS.json", "w") as outfile:
    #         outfile.write(json_object)
    # with open ('conditions_NHS.json', 'w') as f:
    #     json.dump(condition_dict, f)
        
    # with open ('other_NHS.json', 'w') as f:
    #     json.dump(other_dict, f)

def get_vis_data():
    recipes = []
    num = 1
    while num < 300:
        url = "https://worldeatsapi.link/restaurants/"+ str(num)

        payload={}
        headers = {
        'Host': '18.219.58.154'
        }

        response = requests.get(url, headers=headers, data=payload)
        data = json.loads(response.text)
        #print(data)
        city_data = {}
        city_data["name"] = data["name"]
        city_data["food_types"] = data["food_types"]
        city_data["rating"] = data["rating"]
        recipes.append(city_data)
        num+=1

    json_object = json.dumps(recipes, indent=4)
    with open ("restaurants.json", "w") as outfile:
        outfile.write(json_object)


if __name__=="__main__":
    #get_medicines()
    #get_locations()
    #get_data()
    get_vis_data()


