FROM python:3.10
ENV APP_ENV Testing

RUN apt-get update -q -y

COPY . usr/src/backend
COPY requirements.txt usr/src/backend/requirements.txt

WORKDIR /usr/src/backend

RUN pip3 install -r requirements.txt
RUN chmod +x ./start_test.sh