import requests
import json
import os
from unittest import main, TestCase

TREATMENT_SRC = "src/medicationsdata.json"
LOCATION_SRC = "src/hospitalsdata.json"
SYMPTOM_SRC = "src/symptomsdata.json"

env = os.getenv("APP_ENV")
conf = "dev" if env == "Testing" else "localhost"
ENDPOINT = f"http://{conf}:5000"


class Tests(TestCase):
    def test_treatments(self):
        treatments = requests.get(f"{ENDPOINT}/treatments")
        assert treatments.status_code == 200
        file = open(TREATMENT_SRC)
        valid_cnt = len(json.load(file))
        file.close()
        test_cnt = len(treatments.json())
        assert test_cnt == valid_cnt

    def test_treatment_instance(self):
        file = open(TREATMENT_SRC)
        valid = json.load(file)[0]
        file.close()
        treatment = requests.get(f"{ENDPOINT}/treatments/1")
        assert treatment.status_code == 200

        retrieved = treatment.json()
        assert valid["brand_name"] == retrieved["name"]
        assert valid["generic_name"] == retrieved["generic_name"]
        assert valid["manufacturer"] == retrieved["manufacturer"]
        assert valid["form"] == retrieved["form"]
        assert valid["route"] == retrieved["route"]

    def test_treatment_query(self):
        treatment = requests.get(f"{ENDPOINT}/treatments?search=alpra&route=oral")
        assert treatment.status_code == 200

        retrieved = treatment.json()[0]
        assert "Alprazolam Extended Release" == retrieved["name"]
        assert "Alprazolam" == retrieved["generic_name"]
        assert "Bryant Ranch Prepack" == retrieved["manufacturer"]
        assert "TABLET, EXTENDED RELEASE" == retrieved["form"]
        assert "ORAL" == retrieved["route"]

    def test_locations(self):
        locations = requests.get(f"{ENDPOINT}/locations")
        assert locations.status_code == 200
        file = open(LOCATION_SRC)
        valid_cnt = len([i for i in json.load(file)])
        file.close()
        test_cnt = len(locations.json())
        assert test_cnt == valid_cnt

    def test_location_instance(self):
        file = open(LOCATION_SRC)
        valid = json.load(file)[0]
        file.close()
        location = requests.get(f"{ENDPOINT}/locations/1")
        assert location.status_code == 200

        retrieved = location.json()
        assert valid["name"] == retrieved["name"]
        assert valid["address"] == retrieved["address"]
        assert valid["image"] == retrieved["image"]
        assert float(valid["rating"]) == retrieved["rating"]
        assert int(valid["reviews"]) == retrieved["reviews"]

    def test_location_query(self):
        file = open(LOCATION_SRC)
        valid = json.load(file)[60]
        file.close()
        location = requests.get(
            f"{ENDPOINT}/locations?search=houston%20beechnut&rating=2"
        )
        assert location.status_code == 200

        retrieved = location.json()[0]
        assert valid["name"] == retrieved["name"]
        assert valid["address"] == retrieved["address"]
        assert valid["image"] == retrieved["image"]
        assert float(valid["rating"]) == retrieved["rating"]
        assert int(valid["reviews"]) == retrieved["reviews"]

    def test_symptoms(self):
        symptoms = requests.get(f"{ENDPOINT}/symptoms")
        assert symptoms.status_code == 200
        file = open(SYMPTOM_SRC)
        valid_cnt = len(json.load(file))
        file.close()
        test_cnt = len(symptoms.json())

        assert test_cnt == valid_cnt

    def test_symptom_instance(self):
        file = open(SYMPTOM_SRC)
        valid = json.load(file)[0]
        file.close()
        symptom = requests.get(f"{ENDPOINT}/symptoms/1")
        assert symptom.status_code == 200

        retrieved = symptom.json()
        assert valid["name"] == retrieved["name"]
        assert int(valid["severity"]) == int(retrieved["severity"])
        assert int(valid["rarity"]) == int(retrieved["rarity"])
        assert valid["location"] == retrieved["location"]

    def test_symptom_query(self):
        file = open(SYMPTOM_SRC)
        valid = json.load(file)[84]
        file.close()
        symptom = requests.get(
            f"{ENDPOINT}/symptoms?search=psych%20head&rarity=4&severity=3"
        )
        assert symptom.status_code == 200

        retrieved = symptom.json()[0]
        assert valid["name"] == retrieved["name"]
        assert int(valid["severity"]) == int(retrieved["severity"])
        assert int(valid["rarity"]) == int(retrieved["rarity"])
        assert valid["location"] == retrieved["location"]


if __name__ == "__main__":
    main()
