from flask import Flask, Response, request
from models import db, Treatment, Symptom, Location
from init_db import init_db
from flask_cors import CORS
from sqlalchemy.sql import or_
from sqlalchemy import func

import os


def getConfig():
    env = os.getenv("APP_ENV")
    if env == "Testing" or env == "Development":
        return "Development"
    elif env == "Production":
        return "Production"
    else:
        return ""


def create_app():
    app = Flask(__name__)
    CORS(app)
    app.config.from_object(f"config.{getConfig()}Config")
    db.init_app(app)
    return app


app = create_app()

"""
Reinitializes the database
Usage: "flask reinit-db"
"""


@app.cli.command("reinit-db")
def reinit():
    with app.app_context():
        try:
            print(" * Reinitiating database")
            init_db()
            print(" * Reinitialization succeeded")
        except Exception as e:
            # generic error will be refined later
            print(e)
            print(" * Something went wrong")
            raise e


"""
Below are routes for the app
"""


@app.route("/")
def home():
    return "SymptomSolver API"


@app.route("/treatments")
def get_treatments():
    treatments = db.session.query(Treatment)
    toRet = []

    # filtering args
    manufacturer = request.args.get("manufacturer")
    route = request.args.get("route")
    form = request.args.get("form")
    sort = request.args.get("sort")

    # filter by manufacturer
    if manufacturer is not None:
        treatments = treatments.filter(
            (func.lower(Treatment.manufacturer).like(f"%{manufacturer.lower()}%"))
        )

    # filter by route
    if route is not None:
        route = route.lower()
        if route == "oral":
            treatments = treatments.filter((func.lower(Treatment.route) == "oral"))
        elif route == "topical":
            treatments = treatments.filter((func.lower(Treatment.route) == "topical"))
        elif route == "respiratory":
            treatments = treatments.filter(
                (func.lower(Treatment.route) == "respiratory (inhalation)")
            )
        elif route == "intravenous":
            treatments = treatments.filter(
                (func.lower(Treatment.route) == "intravenous")
            )
        elif route == "intrathecal":
            treatments = treatments.filter(
                (func.lower(Treatment.route) == "intrathecal")
            )

    # filter by form
    if form is not None:
        form = form.lower()
        if form == "tablet":
            treatments = treatments.filter((func.lower(Treatment.form) == "tablet"))
        elif form == "gel":
            treatments = treatments.filter((func.lower(Treatment.form) == "gel"))
        elif form == "injection":
            treatments = treatments.filter((func.lower(Treatment.form) == "injection"))
        elif form == "gas":
            treatments = treatments.filter((func.lower(Treatment.form) == "gas"))
        elif form == "capsule":
            treatments = treatments.filter((func.lower(Treatment.form) == "capsule"))
        elif form == "stick":
            treatments = treatments.filter((func.lower(Treatment.form) == "stick"))
        elif form == "powder":
            treatments = treatments.filter(
                (func.lower(Treatment.form) == "powder, for suspension")
            )
        elif form == "jelly":
            treatments = treatments.filter((func.lower(Treatment.form) == "jelly"))
        elif form == "solution":
            treatments = treatments.filter((func.lower(Treatment.form) == "solution"))
        elif form == "aerosol":
            treatments = treatments.filter(
                (func.lower(Treatment.form) == "aerosol, spray")
            )
        elif form == "cream":
            treatments = treatments.filter((func.lower(Treatment.form) == "cream"))

    search = request.args.get("search")
    if search != None:
        search = search.lower().split()
        for k in search:
            clauses = []
            clauses.append(func.lower(Treatment.name).like("%{0}%".format(k)))
            clauses.append(func.lower(Treatment.generic_name).like("%{0}%".format(k)))
            clauses.append(func.lower(Treatment.manufacturer).like("%{0}%".format(k)))
            clauses.append(func.lower(Treatment.form).like("%{0}%".format(k)))
            clauses.append(func.lower(Treatment.route).like("%{0}%".format(k)))
            treatments = treatments.filter(or_(*clauses))

    for treatment in treatments:
        toRet.append(treatment.as_dict())

    # ascend/descend
    if sort is not None and sort == "descending":
        toRet.reverse()
    return toRet


@app.route("/locations")
def get_locations():
    locations = db.session.query(Location)
    toRet = []

    # filtering args
    city = request.args.get("city")
    rating = request.args.get("rating")
    reviews = request.args.get("reviews")
    sort = request.args.get("sort")

    # filter by city
    if city is not None:
        city = city.lower()
        locations = locations.filter((func.lower(Location.address).like(f"%{city}%")))

    # filter by rating
    if rating is not None:
        rating = rating.lower()
        if rating == "1":
            locations = locations.filter((Location.rating >= "1"))
        elif rating == "2":
            locations = locations.filter((Location.rating >= "2"))
        elif rating == "3":
            locations = locations.filter((Location.rating >= "3"))
        elif rating == "4":
            locations = locations.filter((Location.rating >= "4"))
        elif rating == "5":
            locations = locations.filter((Location.rating >= "5"))

    # filter by reviews
    if reviews is not None:
        reviews = reviews.lower()
        if reviews == "10":
            locations = locations.filter((Location.reviews >= "10"))
        elif reviews == "100":
            locations = locations.filter((Location.reviews >= "100"))
        elif reviews == "1000":
            locations = locations.filter((Location.reviews >= "1000"))

    search = request.args.get("search")
    if search != None:
        search = search.lower().split()
        for k in search:
            clauses = []
            clauses.append(func.lower(Location.name).like("%{0}%".format(k)))
            clauses.append(func.lower(Location.address).like("%{0}%".format(k)))
            locations = locations.filter(or_(*clauses))

    for location in locations:
        toRet.append(location.as_dict())

    # asc/desc
    if sort is not None and sort == "descending":
        toRet.reverse()
    return toRet


@app.route("/symptoms")
def get_symptoms():
    symptoms = db.session.query(Symptom)
    toRet = []

    # filtering args
    severity = request.args.get("severity")
    rarity = request.args.get("rarity")
    location = request.args.get("location")
    sort = request.args.get("sort")

    # Filter by severity
    if severity is not None:
        severity = severity.lower()
        if severity == "1":
            symptoms = symptoms.filter((Symptom.severity == "1"))
        elif severity == "2":
            symptoms = symptoms.filter((Symptom.severity == "2"))
        elif severity == "3":
            symptoms = symptoms.filter((Symptom.severity == "3"))

    # Filter by rarity
    if rarity is not None:
        rarity = rarity.lower()
        if rarity == "1":
            symptoms = symptoms.filter((Symptom.rarity == "1"))
        elif rarity == "2":
            symptoms = symptoms.filter((Symptom.rarity == "2"))
        elif rarity == "3":
            symptoms = symptoms.filter((Symptom.rarity == "3"))
        elif rarity == "4":
            symptoms = symptoms.filter((Symptom.rarity == "4"))

    # Filter by location
    if location is not None:
        location = location.lower()
        if location == "arms":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "arms"))
        elif location == "back":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "back"))
        elif location == "bladder":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "bladder"))
        elif location == "blood":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "blood"))
        elif location == "butt":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "butt"))
        elif location == "chest":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "chest"))
        elif location == "ears":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "ears"))
        elif location == "eyes":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "eyes"))
        elif location == "face":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "face"))
        elif location == "feet":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "feet"))
        elif location == "fullbody":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "fullbody"))
        elif location == "genitals":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "genitals"))
        elif location == "hands":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "hands"))
        elif location == "head":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "head"))
        elif location == "hips":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "hips"))
        elif location == "legs":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "legs"))
        elif location == "mouth":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "mouth"))
        elif location == "neck":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "neck"))
        elif location == "nose":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "nose"))
        elif location == "stomach":
            symptoms = symptoms.filter((func.lower(Symptom.location) == "stomach"))

    search = request.args.get("search")
    if search != None:
        search = search.lower().split()
        for k in search:
            clauses = []
            clauses.append(func.lower(Symptom.name).like("%{0}%".format(k)))
            clauses.append(func.lower(Symptom.location).like("%{0}%".format(k)))
            symptoms = symptoms.filter(or_(*clauses))
    for symptom in symptoms:
        toRet.append(symptom.as_dict())

    # asc/desc
    if sort is not None and sort == "descending":
        toRet.reverse()
    return toRet


@app.route("/treatments/<id>")
def get_treatment(id):
    treatment = db.session.get(Treatment, id)
    if treatment == None:
        status = f"Treatment with id {id} not found.\n"
        return Response(status, status=404)
    return treatment.as_dict()


@app.route("/locations/<id>")
def get_location(id):
    location = db.session.get(Location, id)
    if location == None:
        status = f"Location with id {id} not found.\n"
        return Response(status, status=404)
    return location.as_dict()


@app.route("/symptoms/<id>")
def get_symptom(id):
    symptom = db.session.get(Symptom, id)
    if symptom == None:
        status = f"Symptom with id {id} not found.\n"
        return Response(status, status=404)
    return symptom.as_dict()


if __name__ == "__main__":
    app.run(host="0.0.0.0")
