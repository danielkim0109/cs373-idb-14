from models import db, Treatment, Symptom, Location
import json

TREATMENT_SRC = "src/medicationsdata.json"
LOCATION_SRC = "src/hospitalsdata.json"
SYMPTOM_SRC = "src/symptomsdata.json"


def load_treatments():
    file = open(TREATMENT_SRC)
    data = json.load(file)
    file.close()
    result = []
    for treatment in data:
        toAdd = Treatment(
            id=treatment["id"],
            name=treatment["brand_name"],
            generic_name=treatment["generic_name"],
            manufacturer=treatment["manufacturer"],
            form=treatment["form"],
            route=treatment["route"],
            image=treatment["image"],
            related_loc_id=treatment["related_loc_id"],
            related_symp_id=treatment["related_symp_id"],
        )
        result.append(toAdd)
    return result


def load_symptoms():
    file = open(SYMPTOM_SRC)
    data = json.load(file)
    file.close()
    result = []
    for symptom in data:
        result.append(
            Symptom(
                id=symptom["id"],
                name=symptom["name"],
                severity=symptom["severity"],
                rarity=symptom["rarity"],
                location=symptom["location"],
                related_loc_id=symptom["related_loc_id"],
                related_med_id=symptom["related_med_id"],
            )
        )
    return result


def load_locations():
    file = open(LOCATION_SRC)
    data = json.load(file)
    file.close()
    result = []
    for location in data:
        result.append(
            Location(
                id=location["id"],
                name=location["name"],
                address=location["address"],
                rating=location["rating"],
                reviews=location["reviews"],
                image=location["image"],
                related_med_id=location["related_med_id"],
                related_symp_id=location["related_symp_id"],
            )
        )
    return result


"""
init_db()
Erases the database and reloads it with data specified by the src files.
MUST BE CALLED WITHIN APP_CONTEXT

Example {
    with app.app_context():
        init_db()
        ...
}
"""


def init_db():
    global Treatment
    global Symptom
    global Location
    db.drop_all()
    db.create_all()
    db.session.add_all(load_locations())
    db.session.add_all(load_treatments())
    db.session.add_all(load_symptoms())
    db.session.commit()
