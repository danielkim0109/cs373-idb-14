**Postman Documentation**
https://documenter.getpostman.com/view/25877762/2s93Y2Sgq3

**To build and run the development server Dockerfile:**

    1. Install Docker and all of its dependencies
    2. Also install Docker-Compose
    3. Run "docker-compose --profile dev up"
    4. Open up a new terminal in the same folder
    5. Run flask reinit-db
    6. Connect to localhost:5000 to test endpoints

**If you want to run the server locally:**

    1. Install all dependencies in requirements.txt
    2. Install Docker, Docker-Compose, and all of its components.
    3. Run "docker-compose --profile db up" to start the server for PostgreSQL
    4. Run "flask reinit-db"
    5. Run "flask --debug run"
    6. Connect to localhost:5000 to test endpoints

**If you want to test the server in one command:**

    1. Do the Docker stuff
    2. Run "docker-compose --profile test up"

**If you want to test a local copy of the server:**

    1. Install all dependencies in requirements.txt
    2. Install Docker, Docker-Compose, and all of its components.
    3. Run "docker-compose --profile db up" to start the server for PostgreSQL
    4. Run "flask --debug run"
    5. Run "./start_test.sh"
