class Config:
    SECRET_KEY = "temp placeholder"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = (
        "postgresql+psycopg2://{USER}:{PW}@{HOST}:{PORT}/{DB_NAME}".format(
            USER="postgres",
            PW="postgres",
            HOST="localhost",
            PORT=5432,
            DB_NAME="postgres",
        )
    )


class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = (
        "postgresql+psycopg2://{USER}:{PW}@{HOST}:{PORT}/{DB_NAME}".format(
            USER="postgres", PW="postgres", HOST="db", PORT=5432, DB_NAME="postgres"
        )
    )


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://{USER}:{PW}@{HOST}:{PORT}/{DB_NAME}".format(
        USER="postgres",
        PW="postgres",
        HOST="awseb-e-4bwdpjq3gi-stack-awsebrdsdatabase-q0swshp4g61i.cfxxjovdxhmz.us-east-2.rds.amazonaws.com",
        PORT=5432,
        DB_NAME="ebdb",
    )
